#!/bin/bash -e

# Get install path
pyvenvs_install_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd )"

# Uninstall venvs
printf "Destroying all pyvenvs...\n"
for venv_dir in `find $pyvenvs_install_path -mindepth 1 -maxdepth 1 -type d -name '[!.]*'`
do
    venv_name=$(basename $venv_dir)
    venv=$venv_dir/$venv_name-pyvenv
    venv_requirements=$venv_dir/requirements.txt

    rm -rf $venv
done

# Remove symlink in path
printf "Removing pyvenv symlink from path...\n"
sudo rm -rf /usr/local/bin/pyvenv

# Update env file
cat << EOF > $pyvenvs_install_path/.env
PYVENVS_INSTALLED=false
EOF
