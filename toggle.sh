#!/bin/bash -e

# Get install path
pyvenvs_install_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd )"

# Check environment
$pyvenvs_install_path/env_check.sh

# Build dialog command
eval `resize`
pyvenvs_dialog="whiptail --separate-output --title 'Python Virtual Environments' \
--checklist 'Which Python Virtual Environments would you like to enable? \
You can always enable pyvenvs or create your own later on.' \
$(($LINES / 2)) $COLUMNS $(( $LINES / 2 - 8 )) \
'enable_all_pyvenvs' 'Enable and install all pyvenvs' OFF \
'disable_all_pyvenvs' 'Disable and uninstall all pyvenvs' OFF"

# Gather pyvenvs
for pyvenv_dir in `find $pyvenvs_install_path -mindepth 1 -maxdepth 1 -type d -name '[!.]*' | sort`
do
    pyvenv_name=$(basename $pyvenv_dir)
    pyvenv=$pyvenv_dir/$pyvenv_name-pyvenv
    pyvenv_description=$(cat $pyvenv_dir/description.txt)

    enabled_status="OFF"
    if [ -d $pyvenv ]; then
        enabled_status="ON"
    fi

    pyvenvs_dialog=$(printf "$pyvenvs_dialog '$pyvenv_name' '$pyvenv_description' $enabled_status")
done

# Prompt user
pyvenvs_selections=$(eval $pyvenvs_dialog 3>&1 1>&2 2>&3)
disable_all=0
enable_all=0
if [[ $(printf "$pyvenvs_selections\n" | grep -w "disable_all_pyvenvs") ]]; then
    disable_all=1
elif [[ $(printf "$pyvenvs_selections\n" | grep -w "enable_all_pyvenvs") ]]; then
    enable_all=1
fi

# Toggle pyvenvs
for pyvenv_dir in `find $pyvenvs_install_path -mindepth 1 -maxdepth 1 -type d -name '[!.]*' | sort`
do
    pyvenv_name=$(basename $pyvenv_dir)
    pyvenv=$pyvenv_dir/$pyvenv_name-pyvenv
    pyvenv_requirements=$pyvenv_dir/requirements.txt

    if  [ $enable_all -eq 1 ] || [[ $(printf "$pyvenvs_selections\n" | grep -w "$pyvenv_name") ]] && [ $disable_all -eq 0 ]; then
        if [ ! -d $pyvenv ] ; then
            printf "\nInstalling $pyvenv_name in $pyvenv_dir"
            python3 -m venv $pyvenv
            source $pyvenv/bin/activate
            pip install -r $pyvenv_requirements
            deactivate
        fi
    else
        rm -rf $pyvenv
    fi
done
