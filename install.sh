#!/bin/bash -e

# Get install path
pyvenvs_install_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd )"

# Create/update env file
cat << EOF > $pyvenvs_install_path/.env
PYVENVS_INSTALLED=true
EOF

# Check environment
$pyvenvs_install_path/env_check.sh

# Run initial configuration
$pyvenvs_install_path/toggle.sh

# Create symlink in path
sudo ln -sf $pyvenvs_install_path/pyvenv.sh /usr/local/bin/pyvenv
