# 🐍 Python Virtual Environments

A wrapper for managing globally installed Python Virtual Environments.

Pyvenvs is essentially just a group of bash scripts which make managing Python Virtual Environments easier.

Don't pip install outside of a virtual environment. It's just not a good idea in the long run.

## Usage

This information can also be found when running `pyvenv help`

### Installation

Clone the pyvenvs folder as a hidden folder in your home, or somewhere else.

``` bash
git clone https://gitlab.com/Kallahan23/pyvenvs.git ~/.pyvenvs
~/.pyvenvs/install.sh
```

### Activate a pyvenv

Once installed, a pyvenv can be activated using:

``` bash
source pyvenv
```

This will present a dialog box. Select the desired pyvenv to activate it.
Alternatively, if you know the name of the pyvenv you'd like to activate, use:

``` bash
source pyvenv <name>
```

### Create a new pyvenv

To create a fresh, empty pyvenv, use:

``` bash
pyvenv new
```

This will present a series of dialog boxes, and will eventually create an empty virtual environment.

Once completed, run the following to setup and save the requirements for this environment.

``` bash
source pyvenv <name>
pip install <everything you need>
pyvenv save
```

### Uninstall pyvenvs

To delete all the virtual environments and remove the `pyvenv` command from you path, run:

``` bash
pyvenv uninstall
```

This however will not remove the pyvenvs directory. You can always run the install script again.
For a full uninstall, simply delete the pyvenvs folder after running this command.
