# Contributing

If you have a pyvenv you'd like to add to this repo, feel free to submit a merge request.

Running `pyvenv new` to create your environment should set everything up for you, but just to summarise:

Each pyvenv you would like to add should only contain:

- `requirements.txt` with all the requirements listed from pip freeze
- `description.txt` containing a brief description of the environment

These 2 files should be inside a folder. The folder should have the name of the pyvenv.

Have a look at the current repo structure as an example.
