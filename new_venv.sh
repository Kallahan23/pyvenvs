#!/bin/bash -e

# Get install path
pyvenvs_install_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd )"

# Constants
PYVENVS_DIALOG_TITLE="Python Virtual Environments"

# Check environment
$pyvenvs_install_path/env_check.sh

eval `resize`
DIALOG_HEIGHT=$(($LINES / 2))
DIALOG_WIDTH=$COLUMNS

# Get pyvenv name
pyvenv_name=$(whiptail --title "$PYVENVS_DIALOG_TITLE" --inputbox "What would you like to name this virtual environment?" $DIALOG_HEIGHT $DIALOG_WIDTH 3>&1 1>&2 2>&3)
pyvenv_name=$(echo $pyvenv_name | tr '[:upper:]' '[:lower:]')
pyvenv_name=$(echo $pyvenv_name | tr ' ' '-')
pyvenv_dialog_status=$?
if [ $pyvenv_dialog_status -ne 0 ] || [ ! $pyvenv_name ]; then
    printf "No name provided.\nAborting.\n"
    exit
fi

# Get pyvenv description
pyvenv_description=$(whiptail --title "$PYVENVS_DIALOG_TITLE" --inputbox "Please give an optional brief description of what this enviroment would be used for" $DIALOG_HEIGHT $DIALOG_WIDTH 3>&1 1>&2 2>&3)
pyvenv_dialog_status=$?
if [ $pyvenv_dialog_status -ne 0 ]; then
    echo "Cancelled."
    exit
fi

# Create folder and files
mkdir $pyvenvs_install_path/$pyvenv_name
touch $pyvenvs_install_path/$pyvenv_name/requirements.txt
echo $pyvenv_description > $pyvenvs_install_path/$pyvenv_name/description.txt

# Initialise pyvenv
python3 -m venv $pyvenvs_install_path/$pyvenv_name/$pyvenv_name-pyvenv

# Show finish message
whiptail --title "$PYVENVS_DIALOG_TITLE" \
--msgbox "You can now activate your new virtual environment by running 'source pyvenv' and selecting your pyvenv. \
Do this, then 'pip install' everything you need, and finally run 'pyvenv save' to save your requirements." \
$DIALOG_HEIGHT $DIALOG_WIDTH

printf "Please:\n\n  source pyvenv $pyvenv_name\n  pip install <everything you need>\n  pyvenv save\n\n"
