#!/bin/bash -e

# Get install path
pyvenvs_install_path=$(dirname $(readlink -f /usr/local/bin/pyvenv))

# Check environment
$pyvenvs_install_path/env_check.sh

# Check if script was sourced
pyvenvs_sourced=0
if [ -n "$ZSH_EVAL_CONTEXT" ]; then 
    case $ZSH_EVAL_CONTEXT in *:file) pyvenvs_sourced=1;; esac
elif [ -n "$KSH_VERSION" ]; then
    [ "$(cd $(dirname -- $0) && pwd -P)/$(basename -- $0)" != "$(cd $(dirname -- ${.sh.file}) && pwd -P)/$(basename -- ${.sh.file})" ] && pyvenvs_sourced=1
elif [ -n "$BASH_VERSION" ]; then
    (return 0 2>/dev/null) && pyvenvs_sourced=1 
else # All other shells: examine $0 for known shell binary filenames
    # Detects `sh` and `dash`; add additional shell filenames as needed.
    case ${0##*/} in sh|dash) pyvenvs_sourced=1;; esac
fi

# Parse args if not sourced
if [ $pyvenvs_sourced -eq 0 ]; then
    # Check args
    if [ "$1" == "" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ] || [ "$1" == "help" ]; then
        TEXT_RESET="\x1b[0m"
        TEXT_BOLD="\x1b[1m"
        TEXT_FAINT="\x1b[2m"
        TEXT_GREEN="\x1b[32m"
        emoji="🐍"
        if [ ! -z "${SPACESHIP_VENV_SYMBOL// }" ]; then
            emoji=${SPACESHIP_VENV_SYMBOL// }
        fi
        printf "\n$emoji ${TEXT_GREEN}Python Virual Environments${TEXT_RESET}\n\n"
        printf "${TEXT_BOLD}Usage:${TEXT_RESET}\n\n"
        printf "    source pyvenv ${TEXT_FAINT}.............${TEXT_RESET} Activate a virtual environment.\n"
        printf "    source pyvenv <name> ${TEXT_FAINT}......${TEXT_RESET} Activate a specific virtual environment.\n"
        printf "    pyvenv toggle ${TEXT_FAINT}.............${TEXT_RESET} Enable/disable virtual environments.\n"
        printf "    pyvenv new ${TEXT_FAINT}................${TEXT_RESET} Create a new, empty virtual environment.\n"
        printf "    pyvenv save ${TEXT_FAINT}...............${TEXT_RESET} Save pip requirements for active environment.\n"
        printf "    pyvenv list ${TEXT_FAINT}...............${TEXT_RESET} List available virtual environment.\n"
        printf "    pyvenv uninstall ${TEXT_FAINT}..........${TEXT_RESET} Delete all virtual environments and remove pyvenv from path.\n"
        printf "    pyvenv help ${TEXT_FAINT}...............${TEXT_RESET} Show this help message\n"

    elif [ $1 == "toggle" ]; then
        $pyvenvs_install_path/toggle.sh

    elif [ $1 == "new" ]; then
        $pyvenvs_install_path/new_venv.sh
    
    elif [ $1 == "save" ]; then
        if [[ "$VIRTUAL_ENV" != "" ]]; then
            venv_dir=$(dirname $VIRTUAL_ENV)
            pip freeze > $venv_dir/requirements.txt
            printf "The following requirments were saved:\n\n$(cat $venv_dir/requirements.txt)\n"
        else
            echo "You are not currently running a venv. Aborting."
        fi

    elif [ $1 == "list" ]; then
        TEXT_RESET="\x1b[0m"
        TEXT_BOLD="\x1b[1m"
        TEXT_GREEN_FAINT="\x1b[32;2m"
        TEXT_GREEN_BOLD="\x1b[32;1m"
        printf "\n  | ${TEXT_BOLD}Python Virtual Environment${TEXT_RESET} | ${TEXT_BOLD}Status${TEXT_RESET}   |"
        printf "\n  |----------------------------|----------|\n"

        for pyvenv_dir in `find $pyvenvs_install_path -mindepth 1 -maxdepth 1 -type d -name '[!.]*' | sort`
        do
            pyvenv_name=$(basename $pyvenv_dir)
            pyvenv=$pyvenv_dir/$pyvenv_name-pyvenv

            printf "  | %-26s |" $pyvenv_name
            if [ $(env | grep VIRTUAL_ENV | wc -l) -eq 1 ] && [ $VIRTUAL_ENV = $pyvenv ]; then
                printf " ${TEXT_GREEN_BOLD}%-8s${TEXT_RESET} |\n" "Active"
            elif [ -d "$pyvenv" ] && [ -f "$pyvenv/bin/activate" ]; then
                printf " ${TEXT_GREEN_FAINT}%-8s${TEXT_RESET} |\n" "Enabled"
            else
                printf " %-8s |\n" "Disabled"
            fi
        done

    elif [ $1 == "uninstall" ]; then
        echo
        read -p "Are you sure? [yY] " -n 1 -r
        echo
        if [[ $REPLY =~ ^[Yy]$ ]]; then
            $pyvenvs_install_path/uninstall.sh
        fi
    fi

else
    if [ "$1" != "" ] && [ -d "$pyvenvs_install_path/$1" ] && [ -d "$pyvenvs_install_path/$1/$1-pyvenv" ] && [ -f "$pyvenvs_install_path/$1/$1-pyvenv/bin/activate" ]; then
        pyvenvs_selection=$1
        source $pyvenvs_install_path/$pyvenvs_selection/$pyvenvs_selection-pyvenv/bin/activate
    else
        # Build dialog command
        eval `resize`
        pyvenvs_dialog="whiptail --title 'Python Virtual Environments' --menu 'Choose a Python Virtual Environment' $(($LINES / 2)) $COLUMNS $(( $LINES / 2 - 8 ))"
        pyvenvs_enabled=0

        # List venvs
        for pyvenv_dir in `find $pyvenvs_install_path -mindepth 1 -maxdepth 1 -type d -name '[!.]*' | sort`
        do
            if [ -d "$pyvenv_dir/$(basename $pyvenv_dir)-pyvenv" ] && [ -f "$pyvenv_dir/$(basename $pyvenv_dir)-pyvenv/bin/activate" ]; then
                pyvenvs_dialog=$(printf "$pyvenvs_dialog '$(basename $pyvenv_dir)' '$(cat $pyvenv_dir/description.txt)'")
                pyvenvs_enabled=1
            fi
        done

        # Prompt user
        if [ $pyvenvs_enabled -eq 1 ]; then
            pyvenvs_selection=$(eval $pyvenvs_dialog 3>&1 1>&2 2>&3)
            
            # Activate pyvenv
            if [ $? = 0 ]; then
                source $pyvenvs_install_path/$pyvenvs_selection/$pyvenvs_selection-pyvenv/bin/activate
            fi
        else
            printf "No pyvenvs have been enabled. Run 'pyvenv toggle'.\nAborting.\n"
        fi
    fi
fi

# Cleanup
unset pyvenvs_install_path
unset pyvenvs_sourced
unset pyvenvs_dialog
unset pyvenv_dir
unset pyvenvs_selection
unset pyvenvs_enabled
