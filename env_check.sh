#!/bin/bash -e

# Get install path
pyvenvs_install_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd )"

# Check .env
source $pyvenvs_install_path/.env
if [ ! $PYVENVS_INSTALLED = true ] ; then
    printf "Run $pyvenvs_install_path/install.sh first to install pyvenvs.\nAborting.\n"
    exit 1
fi

# Check if python3 installed
if ! hash python3 2>/dev/null
then
    echo "No python3 installation found. Please install python3 before continuing."
    echo "Abort"
    exit 1
fi

# Check if pip3 (python3-pip) installed
if ! hash pip3 2>/dev/null
then
    echo "No pip3 installation found. Please install python3-pip before continuing."
    echo "Abort"
    exit 1
fi

# Check if venv (python3-venv) installed
if ! python3 -c "import venv" > /dev/null 2>&1
then
    echo "No venv installation found. Please install python3-venv before continuing."
    echo "Abort"
    exit 1
fi
